// Copyright (C) 2024 Ahmad Samir <a.samirh78@gmail.com>
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GFDL-1.3-no-invariants-only

//! [q-chrono-timer-alternatives]
    Another alternative is reimplementing the QObject::timerEvent() method
    in your class (which must be a sub-class of QObject), and using one of
    the following approaches:

    \list
    \li Using QBasicTimer, a lightweight value-class wrapping a timer
    ID. You can start the timer with QBasicTimer::start() and stop it with
    QBasicTimer::stop(). You can handle the event in your reimplemneted
    timerEvent().

    \li A more low-level method is manipulating the timer IDs directly.
    To start the timer call QObject::startTimer(), storing the returned
    ID. To stop the timer call QObject::killTimer(). You can handle the event
    in your reimplemented timerEvent(). This approach is typically more
    cumbersome than using QBasicTimer.
    \endlist

    A disadvantage of using timerEvent() is that some high-level features,
    such as single-shot timers and signals, aren't supported.
//! [q-chrono-timer-alternatives]

// \1 is the method argument's name e.g. `\a interval`
//! [negative-intervals-not-allowed]
    Starting from Qt 6.10, setting a negative interval will result in a run-time warning
    and the value being reset to 1ms. Before Qt 6.10 a Qt Timer would let you set a negative
    interval but behave in surprising ways (for example stop the timer if it was running or
    not start it at all).
//! [negative-intervals-not-allowed]
